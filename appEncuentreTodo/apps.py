from django.apps import AppConfig


class AppencuentretodoConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'appEncuentreTodo'
