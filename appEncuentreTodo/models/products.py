from django.db import models
from django.db.models.deletion import  SET_DEFAULT
from .category import Category
from .user import User

class Products(models.Model):
    id                      = models.BigAutoField(primary_key=True)
    product_name            = models.CharField(max_length= 50)
    product_characteristics = models.TextField()# Se crea el atributo caracteristicasProducto que extiende de models con un tipo de dato textual
    product_price           = models.DecimalField(decimal_places=2,max_digits=10,null=True,default=0)# Se crea el atributo precioProducto que extiende de models con un tipo de dato decimal, de maximo 10 numeros y dos numeros despues del punto
    product_state           = models.CharField(max_length=50)
    category                = models.ForeignKey(Category,related_name='products',on_delete=SET_DEFAULT,null=False,default=0)# Se crea el atributo categoria que extiende de models con un tipo de dato llave foranea haciendo referencia a la tabla de categorias
    user                    = models.ForeignKey(User,related_name='products',on_delete=SET_DEFAULT,null=False,default=0)# Se crea el atributo usuario que extiende de models con un tipo de dato llave foranea haciendo referencia a la tabla de user
    
    