from django.db import models
from django.db.models.deletion import CASCADE
from .user import User


class Solicitud(models.Model):
    id             = models.BigAutoField(primary_key=True)
    tipo           = models.CharField(max_length=20)
    fecha          = models.DateField()    
    id_usuario     = models.ForeignKey(User,related_name='solicitud',on_delete=CASCADE,null=False,default=0)
    #id_administrative=
