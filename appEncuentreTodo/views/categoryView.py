from rest_framework.response import Response
from rest_framework.decorators import api_view
from appEncuentreTodo.models.category import Category
from appEncuentreTodo.serializers.categorySerializer import CategorySerializer


@api_view(['GET','POST'])

def category_view(request):
    #Traer de la base de datos
    if request.method == 'GET':
        categories = Category.objects.all()
        categories_serializer = CategorySerializer(categories,many=True)
        return Response(categories_serializer.data)

    #Meter a la base de datos
    elif request.method =='POST':
        cat_serializers=CategorySerializer(data=request.data)
        if  cat_serializers.is_valid():
            cat_serializers.save()
            return Response(cat_serializers.data)
        return Response(cat_serializers.errors)
    



@api_view(['GET','PUT','DELETE'])
def category_unique_view(request,pk = None):
    category = Category.objects.filter(id = pk).first()

    if request.method == 'GET':            
        category_serializer = CategorySerializer(category)
        return Response(category_serializer.data)

    elif  request.method == 'PUT':
        request.data
        category_serializer=CategorySerializer(category,data= request.data)
        if category_serializer.is_valid():
            category_serializer.save()
            return Response(category_serializer.data)
        return Response(category_serializer.errors)

    elif  request.method == 'DELETE':
        category.delete()
        return Response('Eliminado')







