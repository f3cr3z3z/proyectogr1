# Generated by Django 3.2.8 on 2021-10-14 17:11

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('auth', '0012_alter_user_first_name_max_length'),
    ]

    operations = [
        migrations.CreateModel(
            name='User',
            fields=[
                ('last_login', models.DateTimeField(blank=True, null=True, verbose_name='last login')),
                ('is_superuser', models.BooleanField(default=False, help_text='Designates that this user has all permissions without explicitly assigning them.', verbose_name='superuser status')),
                ('id', models.BigAutoField(primary_key=True, serialize=False)),
                ('username', models.CharField(max_length=15, unique=True, verbose_name='Username')),
                ('password', models.CharField(max_length=256, verbose_name='Password')),
                ('name', models.CharField(max_length=30, null=True, verbose_name='Name')),
                ('email', models.EmailField(max_length=100, null=True, verbose_name='Email')),
                ('address', models.CharField(max_length=100, null=True, verbose_name='address')),
                ('phone', models.BigIntegerField(null=True, verbose_name='phone')),
                ('groups', models.ManyToManyField(blank=True, help_text='The groups this user belongs to. A user will get all permissions granted to each of their groups.', related_name='user_set', related_query_name='user', to='auth.Group', verbose_name='groups')),
                ('user_permissions', models.ManyToManyField(blank=True, help_text='Specific permissions for this user.', related_name='user_set', related_query_name='user', to='auth.Permission', verbose_name='user permissions')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Category',
            fields=[
                ('id', models.BigAutoField(primary_key=True, serialize=False)),
                ('name_category', models.CharField(max_length=50, unique=True)),
            ],
        ),
        migrations.CreateModel(
            name='Voucher',
            fields=[
                ('id', models.BigAutoField(primary_key=True, serialize=False)),
                ('voucher_date', models.DateTimeField(auto_now_add=True)),
                ('pay_option', models.CharField(max_length=50)),
                ('send_cost', models.DecimalField(decimal_places=2, default=0, max_digits=10, null=True)),
                ('product_amount', models.IntegerField(max_length=2)),
                ('total_purchace', models.DecimalField(decimal_places=2, default=0, max_digits=10)),
                ('user', models.ForeignKey(default=0, on_delete=django.db.models.deletion.SET_DEFAULT, related_name='voucher', to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='Solicitud',
            fields=[
                ('id', models.BigAutoField(primary_key=True, serialize=False)),
                ('tipo', models.CharField(max_length=20)),
                ('fecha', models.DateField()),
                ('id_usuario', models.ForeignKey(default=0, on_delete=django.db.models.deletion.CASCADE, related_name='solicitud', to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='Products',
            fields=[
                ('id', models.BigAutoField(primary_key=True, serialize=False)),
                ('product_name', models.CharField(max_length=50)),
                ('product_characteristics', models.TextField()),
                ('product_price', models.DecimalField(decimal_places=2, default=0, max_digits=10, null=True)),
                ('product_state', models.CharField(max_length=50)),
                ('category', models.ForeignKey(default=0, on_delete=django.db.models.deletion.SET_DEFAULT, related_name='products', to='appEncuentreTodo.category')),
                ('user', models.ForeignKey(default=0, on_delete=django.db.models.deletion.SET_DEFAULT, related_name='products', to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='DetailVoucher',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False)),
                ('products', models.ForeignKey(default=0, on_delete=django.db.models.deletion.SET_DEFAULT, related_name='detailvoucher', to='appEncuentreTodo.products')),
                ('voucher', models.ForeignKey(default=0, on_delete=django.db.models.deletion.SET_DEFAULT, related_name='detailvoucher', to='appEncuentreTodo.voucher')),
            ],
        ),
    ]
