from appEncuentreTodo.models.category import Category
from appEncuentreTodo.models.products import Products
from rest_framework import serializers

from appEncuentreTodo.models.user import User

class ProductsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Products
        fields = ['id', 'product_name', 'product_characteristics', 'product_price', 'product_state', 'category', 'user']
        
        
    def to_representation(self , obj):
            user = User.objects.get(id=obj.user_id)
            category = Category.objects.get(id=obj.category_id)
                
            product= Products.objects.get(id=obj.id)
            return  {
                    'id': product.id,
                    'product_name': product.product_name,
                    'product_characteristics': product.product_characteristics,
                    'product_price': product.product_price,          
                    'product_state': product.product_state,
                    'category': {'id': product.category.id,
                                'name_category':product.category.name_category
                                
                                },          
                    'user':     {'id': product.user.id,
                                 'name': product.user.name
                                }
                    }