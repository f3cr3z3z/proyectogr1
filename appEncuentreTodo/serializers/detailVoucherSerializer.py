from rest_framework import serializers
from appEncuentreTodo.models.detailvoucher import DetailVoucher
from appEncuentreTodo.models.products import Products
from appEncuentreTodo.models.user import User
from appEncuentreTodo.models.voucher import Voucher

class DetailVoucherSerializer(serializers.ModelSerializer):
    class Meta:
        model = DetailVoucher
        fields= ['id','voucher','products']
        
    def to_representation(self,obj):
            detail = DetailVoucher.objects.get(id=obj.id)
            voucher = Voucher.objects.get(id=obj.voucher_id)
            product= Products.objects.get(id=obj.products_id)
            user = User.objects.get(id=obj.id)
            return  {
                    'id': detail.id,
                    'voucher':  {'id':voucher.id,
                                'voucher_date':voucher.voucher_date,
                                'pay_option': voucher.pay_option,
                                'product_amount': voucher.product_amount,
                                'send_cost':voucher.send_cost,
                                "total_purchace": voucher.total_purchace,
                                'user': {'id':user.id,
                                         'name':user.name
                                        } ,
                                } ,      
                    'products':{'id': product.id,
                                'product_name': product.product_name,
                                'product_state':product.product_state,
                                'product_price':product.product_price 
                                }
                    }    
    