
from rest_framework import serializers
from appEncuentreTodo.models.category import Category

class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model=Category
        
        fields =['id','name_category']