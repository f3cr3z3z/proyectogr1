from django.db.models import fields
from appEncuentreTodo.models.solicitudModel import Solicitud
from rest_framework import serializers
from appEncuentreTodo.models.user import User

class SolicitudSerializer(serializers.ModelSerializer):
    class Meta:
        model  = Solicitud
        fields = ['id','tipo','fecha','id_usuario']