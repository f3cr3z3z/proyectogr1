from rest_framework import serializers
from appEncuentreTodo.models.user import User


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['id','username','password','name','email','address','phone']
        
        
        def to_representation(self,obj):
            user = User.objects.get(id=obj.id)
            return  {
                    'id': user.id,
                    'username': user.username,
                    'name': user.name,
                    'email': user.email,
                    'address': user.address,
                    'phone': user.phone
                    }
                    