"""authPrEncuentreTodo URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from rest_framework_simplejwt.views import (TokenObtainPairView, TokenRefreshView)
from appEncuentreTodo import views
from appEncuentreTodo.views.voucherView import voucher_view,voucher_unique_view
from appEncuentreTodo.views.categoryView import category_view,category_unique_view
from appEncuentreTodo.views.detailVoucherView import detail_voucher_view,detail_voucher_unique_view
from appEncuentreTodo.views.productsView import product_view,product_unique_view

urlpatterns = [
    #path('admin/', admin.site.urls),
    path('login/', TokenObtainPairView.as_view()),
    path('refresh/', TokenRefreshView.as_view()),
    path('user/', views.UserPostView.as_view()),
    path('user/<int:pk>/',views.UserGetView.as_view()),
    path('user/update/<int:pk>/', views.UserPutView.as_view()),
    path('user/del/<int:pk>/',views.UserDeleteView.as_view()),
    path('voucher/',voucher_view),
    path('voucher/<int:pk>/',voucher_unique_view),
    path('voucherDetail/',detail_voucher_view),
    path('voucherDetail/<int:pk>/',detail_voucher_unique_view),
    path('category/',category_view),
    path('category/<int:pk>/',category_unique_view),  
    path('product/', product_view),
    path('product/<int:pk>', product_unique_view)    
]
