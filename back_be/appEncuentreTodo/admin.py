from django.contrib import admin
from .models.voucher import Voucher
from .models.user import User
from .models.category import Category
from .models.products import Products
from .models.detailvoucher import DetailVoucher
from .models.solicitudModel import Solicitud


admin.site.register(User)
admin.site.register(Voucher)
admin.site.register(Products)
admin.site.register(Category)
admin.site.register(DetailVoucher)
admin.site.register(Solicitud)