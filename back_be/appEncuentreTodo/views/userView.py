from django.conf import settings
from rest_framework import generics, status,views
from rest_framework.response import Response
from rest_framework_simplejwt.backends import TokenBackend
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer
from rest_framework.permissions import IsAuthenticated
from appEncuentreTodo.models.user import User
from appEncuentreTodo.serializers.userSerializers import UserSerializer


class UserPostView(views.APIView):
    
    def post(self, request, *args, **kwargs):
        serializer = UserSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        tokenData = {"username":request.data["username"],
                    "password":request.data["password"]}
        tokenSerializer = TokenObtainPairSerializer(data=tokenData)
        tokenSerializer.is_valid(raise_exception=True)
        return Response(tokenSerializer.validated_data, status=status.HTTP_201_CREATED)

    
        
    
class UserGetView(generics.RetrieveAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer
    permission_classes = (IsAuthenticated,)
    def get(self, request, *args, **kwargs):
        token = request.META.get('HTTP_AUTHORIZATION')[7:]
        tokenBackend = TokenBackend(algorithm=settings.SIMPLE_JWT['ALGORITHM'])
        valid_data = tokenBackend.decode(token,verify=False)
        if valid_data['user_id'] != kwargs['pk']:
            stringResponse = {'detail':'Unauthorized Request'}
            return Response(stringResponse, status=status.HTTP_401_UNAUTHORIZED)
        return super().get(request, *args, **kwargs)

class UserPutView(views.APIView):
    def put(self, request,pk=None,*args, **kwargs):
            llave = User.objects.filter(id = pk).first()                     
            #if request.method == 'PUT':
            request.data
            serializer = UserSerializer(llave,data=request.data)
            if serializer.is_valid(raise_exception=True):
                    serializer.save()
                    return Response(serializer.data)            
            return Response(serializer.errors)
        
class UserDeleteView(views.APIView):
    def delete(self, request, pk = None):
        post = User.objects.filter(id = pk).first()
        post.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


    





    
    
    
    
