from rest_framework.response import Response
from rest_framework.decorators import api_view
from appEncuentreTodo.models.voucher import Voucher
from appEncuentreTodo.serializers.voucherSerializer import VoucherSerializer

@api_view(['GET','POST'])
def voucher_view(request):
    if request.method =='GET':
        vouchers=Voucher.objects.all()
        vouchers_serializer = VoucherSerializer(vouchers,many=True)
        return Response (vouchers_serializer.data)
    
    elif request.method == 'POST':
        voucher_serializers=VoucherSerializer(data=request.data)
        if  voucher_serializers.is_valid():
            voucher_serializers.save()
            return Response(voucher_serializers.data)
        return Response(voucher_serializers.errors)
    
    
@api_view(['GET','PUT','DELETE'])
def voucher_unique_view(request,pk=None):
    voucher = Voucher.objects.filter(id=pk).first()
    if request.method == 'GET':
        voucher_serializer = VoucherSerializer(voucher)
        return Response(voucher_serializer.data)

    
    
    elif request.method == 'DELETE':
        voucher.delete()
        return Response('Eliminado')
    
    