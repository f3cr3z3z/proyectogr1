from rest_framework.response import Response
from rest_framework.decorators import api_view
from appEncuentreTodo.models.detailvoucher import DetailVoucher
from appEncuentreTodo.serializers.detailVoucherSerializer import DetailVoucherSerializer



@api_view(['GET','POST'])
def detail_voucher_view(request):
    if request.method == 'GET':
        det_voucher= DetailVoucher.objects.all()
        det_voucher_serializer=DetailVoucherSerializer(det_voucher,many = True)
        return Response( det_voucher_serializer.data)
    elif request.method == 'POST':
        detail_voucher_serializer=DetailVoucherSerializer(data=request.data)
        if detail_voucher_serializer.is_valid():
            detail_voucher_serializer.save
            return Response (detail_voucher_serializer.validated_data)
        return Response(detail_voucher_serializer.errors)
    
    
@api_view(['GET','PUT','DELETE'])
def detail_voucher_unique_view(request,pk = None):
    detail_voucher=DetailVoucher.objects.filter(id = pk)
    if request.method == 'GET':
        detail_voucher_Seri=DetailVoucherSerializer(detail_voucher)
        return Response(detail_voucher_Seri.data)
    elif request.method == 'PUT':
         detail_serializer_voucher = DetailVoucherSerializer(detail_voucher,data = request.data)
         if detail_serializer_voucher.is_valid():
             detail_serializer_voucher.save()
             return Response(detail_serializer_voucher.validated_data)
         return Response(detail_serializer_voucher.errors)
    elif request.method == 'DELETE':
        detail_voucher.delete
        return Response('DELETE')        


