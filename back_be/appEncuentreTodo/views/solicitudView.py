from rest_framework.response import Response
from rest_framework.decorators import api_view
from appEncuentreTodo.models.solicitudModel import Solicitud
from appEncuentreTodo.serializers.solicitudSerializer import SolicitudSerializer
from rest_framework import status


@api_view(['GET','POST'])

def solicitud_view(request):
    if request.method == 'GET':
        solicitud = Solicitud.objects.all()
        solicitud_serializer =SolicitudSerializer(solicitud,many=True)
        return Response(solicitud_serializer.data)

    elif request.method == 'POST':
        solicitud_serializer = SolicitudSerializer(data=request.data)
        if  solicitud_serializer.is_valid():
            solicitud_serializer.save()
            return Response(solicitud_serializer.data)
        return Response(solicitud_serializer.errors) 


@api_view(['GET','DELETE'])
def solicitud_unique_view(request,pk=None):
    solicitud = Solicitud.objects.filter(id=pk).first()

    if request.method == 'GET':            
        solicitud_serializer = SolicitudSerializer(solicitud)
        return Response(solicitud_serializer.data)

    elif  request.method == 'DELETE':
        solicitud.delete()
        return Response('Eliminado')


