from rest_framework.response import Response
from rest_framework.decorators import api_view
from appEncuentreTodo.models.products import Products
from appEncuentreTodo.serializers.productsSerializer import ProductsSerializer

@api_view(['GET','POST'])
def product_view(request):
    if request.method =='GET':
        products = Products.objects.all()
        products_serializer = ProductsSerializer(products,many=True)
        return Response (products_serializer.data)
    
    elif request.method =='POST':
        pro_serializer = ProductsSerializer(data=request.data)
        if  pro_serializer.is_valid():
            pro_serializer.save()
            return Response(pro_serializer.data)
        return Response(pro_serializer.errors)
    
    
@api_view(['GET','PUT','DELETE'])
def product_unique_view(request,pk=None):
    product = Products.objects.filter(id=pk).first()
    if request.method == 'GET':
        product_serializer = ProductsSerializer(product)
        return Response(product_serializer.data)

    elif request.method == 'PUT':
        request.data
        product_serializers = ProductsSerializer(product,data=request.data)
        if  product_serializers.is_valid():
            product_serializers.save()
            return Response(product_serializers.data)
        return Response(product_serializers.errors)
    
    elif request.method == 'DELETE':
        product.delete()
        return Response('Eliminado')

