from .user import User
from .voucher import Voucher
from .category import Category
from .products import Products
from .detailvoucher import DetailVoucher
from .solicitudModel import Solicitud