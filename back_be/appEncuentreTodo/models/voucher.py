from django.db import models
from django.db.models.deletion import   SET_DEFAULT

from .user import User




class Voucher(models.Model):
    id              = models.BigAutoField(primary_key=True)
    user            = models.ForeignKey(User,related_name='voucher',on_delete=SET_DEFAULT,null=False,default=0)
    voucher_date    = models.DateTimeField(auto_now_add=True)# Se crea el atributo fechaFactura que extiende de models con un tipo de dato de fecha, establecida segun la actual
    pay_option      = models.CharField(max_length=50,null=False)
    send_cost       = models.DecimalField(decimal_places=2,max_digits=10,null=True,default=0)
    product_amount  = models.IntegerField(max_length=2,null=False)# Se crea el atributo cantidadProductos(compra) que extiende de models y tiene un tipo de dato entero de dos digitos 
    total_purchace  = models.DecimalField(decimal_places=2,max_digits=10,null=False,default=0)
    
    
 