from django.db import models
from .products import Products
from .voucher import Voucher
from django.db.models.deletion import  SET_DEFAULT
class  DetailVoucher(models.Model):
    id          = models.AutoField(primary_key=True)
    voucher     =models.ForeignKey(Voucher,related_name='detailvoucher',on_delete=SET_DEFAULT,null= False,default=0)
    products    = models.ForeignKey(Products,related_name='detailvoucher',on_delete=SET_DEFAULT,null= False,default=0)
    

