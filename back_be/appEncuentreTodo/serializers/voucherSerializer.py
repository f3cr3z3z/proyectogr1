from rest_framework import serializers
from appEncuentreTodo.models.detailvoucher import DetailVoucher
from appEncuentreTodo.models.voucher import Voucher

class VoucherSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = Voucher
        fields = ['id','user','voucher_date','pay_option','send_cost','product_amount','total_purchace']
    
        
        
    
        
       
                    